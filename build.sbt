organization   := "gov.nist"

name           := "xml-util"

version        := "1.0.0"

crossPaths := false

libraryDependencies ++= Seq(
  "xom"          %     "xom"          %    "1.2.5"
)

